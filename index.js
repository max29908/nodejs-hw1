const express = require('express')
const path = require('path')
const fs = require('fs')

const PORT = 8080
const app = express()


const filePath = path.join(__dirname, 'files')
try {
    if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath)
    }
} catch (err) {
    console.log(err)
}
const validExtensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js']

app.use(express.json())

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next()
})


app.post('/api/files', createFile)
app.get('/api/files', getFiles)

app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename
    fs.readdir(filePath, (err, files) => {
        if (err) {
            logger(req, res, 'Client error')
            res.status(400).json({message: 'Client error'})
        } else if (!files.find(file => file === filename)) {
            logger(req, res, `No file with ${filename} filename found`)
            res.status(400).json({message: `No file with ${filename} filename found`})
        } else {
            const parsed = path.parse(path.join(filePath, filename), err => {
                if (err) {
                    logger(req, res, 'Server error')
                    res.status(500).json({message: 'Server error'})
                }})
            let birthtime = null
            fs.stat(path.join(filePath, filename), (err, stats) => {
                if (err) {
                    logger(req, res, 'Server error')
                    res.status(500).json({message: 'Server error'})
                }
                birthtime = stats.birthtime
            })
            console.log(birthtime)
            fs.readFile(path.join(filePath, filename), (err, content) => {
                if (err) {
                    logger(req, res, 'Server error')
                    res.status(500).json({message: 'Server error'})
                } else {
                    logger(req, res, 'Success')
                    res.status(200).json({
                        message: 'Success',
                        filename: parsed.base,
                        content: content.toString(),
                        extension: parsed.ext.slice(1),
                        uploadedDate: birthtime
                    })
                }

            })
        }
    })
})



app.listen(8080, () => {
    console.log(`Glory to Ukraine! \nRussian warship is going to fuck himself on port ${PORT}...`)
    console.log(filePath)
})

function createFile(req, res) {
    const filename = req.body.filename
    if (!filename) {
        logger(req, res, 'Invalid "filename" parameter')
        res.status(400).json({message: 'Please specify "filename" parameter'})
    }
    if (!validExtensions.find(elem => elem === path.extname(req.body.filename))) {
        logger(req, res, 'Invalid file extension')
        res.status(400).json({message: 'Invalid file extension'})
    } else {
        fs.readdir(filePath, (err, files) => {
            if (err) {
                logger(req, res, 'Client error')
                res.status(500).json({message: 'Server error'})
            } else if (files.find(file => file === filename)) {
                logger(req, res, 'File already exists')
                res.status(400).json({message: 'File already exists'})
            } else if (!req.body.content) {
                logger(req, res, 'Invalid "content" parameter')
                res.status(400).json({message: 'Please specify "content" parameter'})
            } else {
                fs.writeFile(path.join(filePath, filename), req.body.content, err => {
                    if (err) {
                        logger(req, res, 'Server error')
                        res.status(500).json({message: 'Server error'})
                    } else {
                        logger(req, res, 'File created successfully')
                        res.status(200).json({message: 'File created successfully'})
                    }
                })
            }
        })
    }
}

function getFiles(req, res) {
    fs.readdir(filePath, (err, files) => {
        if (err) {
            logger(req, res, 'Server error')
            res.status(500).json({message: 'Server error'})
        } else if (!files.length) {
            logger(req, res, 'Client error')
            res.status(400).json({message: 'Client error'})
        } else {
            logger(req, res, 'Success')
            res.status(200).json({message: 'Success', files: files})
        }
    })
}

function logger(req, res, message) {
    console.log(`${new Date(Date.now()).toLocaleTimeString()} LOG: ${req.method}:${req.originalUrl} - ${message}`)
}

